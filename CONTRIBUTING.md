# Contributing to UXLS

We're always looking for help improving the design, content and code of the UXLS website.

## Contribute using Git

* never push to `master` and `dev`!
* for each feature you'd like to implement, create an own feature branch from `dev` and work on that. As soon (!) as you are finished, open a merge request. Till will merge it back into `dev`.
* Commit early and often. Don't let your feature branch drift apart from `dev` too much, or else madness will ensue.

## Requirements

You may run a dev environment using Docker compose. Install Docker on your system and just enter `docker-compose up` in your terminal.

For development only. Content editing and deployment will only require the single binary of Hugo, which does NOT require any dependencies.

* [Hugo](https://gohugo.io) - static site generator
* [nodejs](https://nodejs.org) - javascript interpreter, comes with npm, a packet manager - use the LTS version, the current 8.0 will break dependencies
* [Ruby](https://www.ruby-lang.org) - ruby interpreter, required for sass
* [Sass](https://sass-lang.com) - a CSS extension language

The following tools are npm packages that should be installed globally with `npm install -g _packagename_`:

* [gulp-cli](https://gulpjs.com) - the task runner for building and watching css/js

## Dev Setup

First install the development dependencies, obviously. Everything you need to work on the theme is found in the `themes/pa-uxls` directory. After cloning the repository, you have to install the dev dependencies: simply run `npm install` in the base directory, where packages.json is located. The necessary packages are defined in this file and will be installed automatically. The packet management will complain on any error. And yes, that magnificent tree view is showing dependencies of dependencies of dependencies, and so on... 

Additional front end components, i.e. everything that will be deployed by hugo, can be installed through node.js. The gulpfile will probably receivesome additional tweaking (see comment and requirements for details.)

Since all styles, scripts and assets are managed by gulp and node.js, keep in mind that none of the files in the static directory should be edited directly. The CSS can be inspected with the provided map file, but changes must only be made directly to the source files in the theme directory.

Gulp, when executed, will compile all necessary files in the `themes/static`-directory, the place where these things belong in a hugo theme. Simply run `gulp` for a whole clean and build or "gulp watch" for a live directory watcher, that will currently execute the gulp tasks `sass` and `scripts` every time stylesheets or javascript files are changed or added in the src directory. Additional resources can be added, though not all are necessarily needed in the watcher task, e.g. fonts or static images and other parts that are rarely changed while building the theme.

The idea is to have two running tools for full theme development: `gulp watch` (in the base directory, where gulpgile is located) and `hugo serve` (in UXLS, the location of hugo's `config.toml`). Gulp will watch for changes to the styling and produce output, hugo will watch for changes in the template and content directories, will render static web pages and liveupdate a local webserver to preview the website output - which it does amazingly fast.

## Working with Hugo

There is a comprehensive official documentation (gohugo.io), I would recommend [_Tutorials/Creating a New Theme_](http://gohugo.io/tutorials/creating-a-new-theme/) for an introduction to the basics.