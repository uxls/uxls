+++
bio = ""
company = "Amgen"
country = ""
date = "2017-06-17T16:46:59+01:00"
email = ""
featured = "yes"
firstname = ""
image = ""
lastname = ""
mobile = ""
name = "Joel Miller"
phone = ""
role = ""
summary = "Joel is a User Experience Lead at Amgen, one of the world’s leading biopharmaceutical companies. Joel has been a vocal and influential user-centered design practitioner throughout his career in the Life Sciences industry."
tags = []
thumbnail = "joel-miller.jpg"
title = "Joel Miller"
weight = "200"
+++

