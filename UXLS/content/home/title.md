+++
date = "2017-06-06T15:33:40+03:00"
weight = "100"
widget = "title"
image = "home-title.jpg"
+++

# A UX Toolkit for the Life Sciences Community,<br>by the Life Sciences Community

We are a community of User Experience (UX) practitioners from the pharmaceutical, biotechnology, and software industries. We created the UX for Life Sciences Toolkit to enable businesses to adopt UX principles and methods as they develop scientific software.

<div class="text-center">{{< cta title="View all methods" ref="methods" style="btn-primary" >}}</div>
