+++
date = "2017-06-07T15:41:11+02:00"
weight = "180"
widget = "organisations"
imagedir = "home/steering-committee"
+++

## Created by leading life sciences organizations

The UX methods have been created by UX professionals from leading companies in the pharmaceutical, healthcare and scientific software industries.
