+++
date = "2017-06-07T15:41:26+02:00"
weight = "110"
widget = "methods"

# TODO: add parameters for the icons.
coveringtitle = "What’s in the toolkit?"
covering = [
	"Why UX matters for life sciences",
	"How-to instructions for UX methods",
	"Tips for life sciences",
	"Templates and resources",
	"Life science case studies showing the methods in action"
]

peopletitle = "Tailored for users in life sciences"
people = [
"User Experience (UX) practitioners",
"Business analysts",
"Software developers",
"Managers of technical delivery teams",
"Anyone looking to apply UX principles and methods to life sciences projects"
]
+++
