+++
date = "2017-11-03T00:00:00+00:00"
title = "UX Approach & Principles"
image = "approach-and-principles/banner.png"
pageclass = "ux-approach-and-principles"

[menu.main]
parent = "guide-to-ux"
weight = 110

[[sidebar]]
    title = "UX Methods in Action"
    type = "case-studies"

[[sidebar]]
    title = "Latest Methods"
    type = "methods"
+++

## UX Approach

A UX approach involves deeply understanding the users through research, organizing information, visual design (and more), all with the goal of meeting user needs and doing it elegantly. It involves putting users at the centre of the design and development process, and establishing an iterative cycle of research, design and evaluation.

{{<cycle 
    title1="Research" 
    desc1="Perform initial user research to help understand the target audience (capabilities, limitations, goals, expectations) and their tasks."
    title2="Design"
    desc2="User research insights are used to help generate ideas and early designs. Prototyping can be used to bring concepts to life."
    title3="Evaluation"
    desc3="It's important to capture user feedback, and to measure the delivered UX throughout a project, not just at the end."
>}}


---

## Key UX Principles

{{<principles>}}

{{<principle title="Understand Needs" image="/guide-to-ux/approach-and-principles/icon-understand-needs.png">}}
It's important to capture and understand all the critical needs of users.
{{</principle>}}

{{<principle title="Data-driven" image="/guide-to-ux/approach-and-principles/icon-data-driven.png">}}
Make informed design decisions based on user derived evidence, not opinion.
{{</principle>}}

{{<principle title="User-focused" image="/guide-to-ux/approach-and-principles/icon-user-focused.png">}}
Know your users and get them involved as early as possible.
{{</principle>}}

{{<principle title="Iterate" image="/guide-to-ux/approach-and-principles/icon-iterate.png">}}
Establish a cycle of research, design and evaluation, and iterate this cycle.
{{</principle>}}

{{</principles>}}

---

## UX Principles for the Life Sciences

Good UX is more than just taking a UX approach, it's about having a UX mindset as well. Follow these key UX principles to help guide your thinking.

{{<principles>}}

{{<principle title="Observe in Context" image="/guide-to-ux/approach-and-principles/icon-observe-in-context.png">}}
Observe and understand the specific location(s) and conditions in which the solutions will be used.
{{</principle>}}

{{<principle title="Factor Complexity" image="/guide-to-ux/approach-and-principles/icon-factor-complexity.png">}}
Understand how the solution fits into overall work practices and discovery process, including collaboration.
{{</principle>}}

{{<principle title="Examine Tools & Data" image="/guide-to-ux/approach-and-principles/icon-examine-tools-data.png">}}
Look at the full range of tools, equipment and data used in conjunction with the solution.
{{</principle>}}

{{<principle title="Innovate" image="/guide-to-ux/approach-and-principles/icon-innovate.png">}}
Identify opportunities to innovate, while being mindful of the need to balance against existing SOPs and established practices.
{{</principle>}}

{{</principles>}}