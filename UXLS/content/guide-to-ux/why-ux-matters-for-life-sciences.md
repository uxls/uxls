+++
date = "2017-06-01T12:54:38+02:00"
title = "Why UX Matters for Life Sciences"
pageclass = "ux-approach-and-principles"

[menu.main]
parent = "guide-to-ux"
weight = 100
+++


## What is UX?

User Experience (UX) encompasses the end-user’s interaction with a product, tool, or system. It involves deeply understanding users through research, organizing information, visual design (and more), all with the goal of meeting user needs and doing it elegantly. This is especially important in the life sciences, where users are scientists in a complex environment and more usable, efficient tools can accelerate innovation.



## Why does UX matter?

- **It drives user adoption, happiness, and task success**\\
A positive user experience makes for happier, more efficient scientists.

- **It provides real data to make better design decisions**\\
Designs are driven by more than just opinions.

- **It saves time and money in the long run**\\
An effective design can reduce the need for training, user support and rework.

Empathizing with users in context ensures that teams are solving the right problem. Learning how users understand a tool simplifies decisions in product development – everything from designing a button to be easily found, to integrating a new feature into their workflow.
