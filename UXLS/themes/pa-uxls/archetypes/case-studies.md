+++
author = ""
company = ""
featured = false
method = ""
summary = ""

[menucontent]
image = ""

[menu.main]
parent = ""

[[sidebar]]
    title = "Team"
    content = ["2 UX researchers", "2 bench scientists"]

[[sidebar]]
    title = "Timeline"
    content = ["3-4 weeks"]

[[sidebar]]
    title = "Deliverables"
    content = ["Analysis Report (.ppt)"]
+++
 

{{< section sidebar="true" >}}
## Introduction
{{< /section >}}

{{< section>}}
## Process
{{< /section >}}
 
 {{< section>}}
### Plan
{{< /section >}}

{{< section>}}
### Conduct
{{< /section >}}

{{< section>}}
### Analyze and Report
{{< /section >}}

{{< section>}}
## Outcome
{{< /section >}}
